﻿using UnityEngine;

public class ShakeOnClick : MonoBehaviour
{
    public CameraShake Camera;
    public CameraShakeEventData data;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) Camera.AddShakeEvent(data);
    }
}